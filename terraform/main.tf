terraform {
  required_version = ">=0.13"
}

provider "aws" {
  region = var.aws_region
}

resource "aws_key_pair" "auth" {
  key_name   = var.key_name
  public_key = file(var.public_key_path)
}

data "aws_ami" "bps" {
  most_recent = true

  filter {
    name   = "name"
    values = ["buildah-podman-skopeo-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["self"]
}

resource "aws_security_group" "gitlab_runner" {
  name        = "gitlab_runner"
  description = "Allow traffic for GitLab runner"

  ingress {
    description = "Allow SSH access"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "sg-${var.tags.Environment}"
    Owner       = var.tags.Owner
    Environment = var.tags.Environment
  }
}

resource "aws_eip" "default" {
  count    = var.runner_count
  instance = element(aws_instance.gitlab_runner.*.id, count.index)
  vpc      = true

  tags = {
    Name        = "eip-${var.tags.Environment}-${count.index + 1}"
    Owner       = var.tags.Owner
    Environment = var.tags.Environment
  }
}

resource "aws_instance" "gitlab_runner" {
  count                       = var.runner_count
  ami                         = data.aws_ami.bps.id
  instance_type               = var.instance_type
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.gitlab_runner.id]
  key_name                    = aws_key_pair.auth.id

  tags = {
    Name        = "gitlab-runner-buildah-podman-skopeo"
    Owner       = var.tags.Owner
    Environment = var.tags.Environment
  }
}
