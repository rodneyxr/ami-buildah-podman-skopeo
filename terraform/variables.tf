variable "aws_region" {
  description = "AWS region to launch servers"
  default     = "us-east-1"
}

variable "instance_type" {
  description = "AWS instance type"
  default     = "t2.medium"
}

variable "runner_count" {
  description = "The number of runners to launch"
  default     = "1"
}

variable "key_name" {
  description = "Desired name of AWS key pair"
  default     = "gitlab-runner"
}

variable "public_key_path" {
  description = <<DESCRIPTION
Path to the SSH public key to be used for authentication.
Ensure this keypair is added to your local SSH agent so provisioners can
connect.
Example: ~/.ssh/id_rsa.pub
DESCRIPTION
  default     = "~/.ssh/id_rsa.pub"
}

variable "tags" {
  default = {
    Owner       = "rrodriguez"
    Environment = "gitlab-runner"
  }
}
