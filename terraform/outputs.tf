resource "local_file" "ansible_inventory" {
  content = templatefile("inventory.tmpl",
    {
      runner-id = aws_eip.default.*.id,
      runner-dns = aws_eip.default.*.public_dns,
      runner-public-ip = aws_eip.default.*.public_ip,
    }
  )
  filename = "inventory"
}