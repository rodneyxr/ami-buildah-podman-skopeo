# ami-buildah-podman-skopeo
This will deploy gitlab-runner instances to AWS. Runner registration will need to be done manually for now.
The runner instances are based on RHEL8 AMI and have buildah, podman, skopeo and gitlab-runner installed.

# Install Requirements
* Ansible - https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html
* AWS CLI - https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html
* Packer - https://learn.hashicorp.com/packer/getting-started/install
* Terraform - https://learn.hashicorp.com/terraform/getting-started/install.html

## AMI build instructions
```bash
cd packer
packer build main.json
```

## Deploy a gitlab runner (not registered)
```bash
export TF_VAR_public_ssh_key=~/.ssh/gitlab-runner.pub
cd terraform
terraform init
terraform plan -out terraform.plan
terraform apply "terraform.plan"
```

## Destroy gitlab runners
```bash
cd terraform
terraform destroy -state terraform.tfstate
```